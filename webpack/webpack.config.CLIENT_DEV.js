const path = require(`path`);
const main = require(`./webpack.config.CLIENT.js`);
const merge = require(`webpack-merge`);
// const HtmlWebPackPlugin = require(`html-webpack-plugin`);

module.exports = merge(main, {
    mode: `development`,
    devtool: `cheap-eval-source-map`,
    output: {
        path: `${process.env.PWD}/dev/public`,//path.resolve(__dirname, `dev`),
        filename: `[name]-[contentHash].js`
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                use: {
                    loader: `html-loader`,
                    options: {
                        minimize: false,
                        removeComments: false,
                        collapseWhitespace: false,
                        root: path.resolve(__dirname),
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    `style-loader`, // injects js-ified css to dom
                    `css-loader`, // injects css into js
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    `style-loader`, // injects js-ified css to dom
                    `css-loader`, // injects css into js
                    // `sass-loader`, // compiles sass to css
                    {
                        loader: `sass-loader`,// compiles Sass to CSS
                        options: {
                            includePaths: [
                                `./node_modules/bourbon/app/assets/stylesheets`,
                            ]
                        }
                    }
                ]
            }
        ]
    },
});