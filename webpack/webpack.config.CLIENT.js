// const path = require(`path`);
const HtmlWebPackPlugin = require(`html-webpack-plugin`);
const CleanWebpackPlugin = require(`clean-webpack-plugin`);

module.exports = {
    entry: {
        'App': `./src/client/App.tsx`,
        // init: `./src/init.ts`,
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: `ts-loader`,
                exclude: /node_modules/
            },
            {
                test: /\.(svg|png|jpg|gif)$/,
                exclude: /favicon..../,
                use: {
                    loader: `file-loader`,
                    options: {
                        name: `[name]-[hash].[ext]`,
                        outputPath: `_assets/`
                    }
                }
            },
            {
                test: /\.(ico)$/,
                use: {
                    loader: `file-loader`,
                    options: {
                        name: `[name].[ext]`,
                        outputPath: `/`
                    }
                }
            },
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: `./src/client/index.html`,
            filename: `./index.html`,
            chunks: [`App`,/* `init`,*/],
        }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [`**/*`, `!favicon.*`],
            cleanAfterEveryBuildPatterns: [`!favicon.*`]
        }),
    ],
    resolve: {
        extensions: [`.ts`, `.tsx`, `.js`]
    }
};