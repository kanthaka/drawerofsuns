// const path = require(`path`);
const HtmlWebPackPlugin = require(`html-webpack-plugin`);
const CleanWebpackPlugin = require(`clean-webpack-plugin`);

module.exports = {
    entry: {
        'server': `./src/server.js`,
        // init: `./src/init.ts`,
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: `ts-loader`,
                exclude: /node_modules/
            },
        ]
    },
    plugins: [
    ],
    resolve: {
        extensions: [`.ts`, `.tsx`, `.js`]
    }
};