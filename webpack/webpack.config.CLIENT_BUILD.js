const path = require(`path`);
const main = require(`./webpack.config.CLIENT.js`);
const merge = require(`webpack-merge`);
// const HtmlWebPackPlugin = require(`html-webpack-plugin`);
const MiniCssExtractPlugin = require(`mini-css-extract-plugin`);
const OptimizeCssAssetsPlugin = require(`optimize-css-assets-webpack-plugin`);
// const UglifyJsPlugin = require(`uglifyjs-webpack-plugin`);
const TerserPlugin = require(`terser-webpack-plugin`);

module.exports = merge(main, {
    mode: `production`,
    devtool: `none`,
    target: `web`,
    output: {
        path: `${process.env.PWD}/build/public`,//path.resolve(__dirname, `build`),//process.env.PWD,//(path.resolve(__dirname(module.parent.filename))),
        filename: `[name]-[contentHash].js`
    },
    optimization: {
        minimizer: [
            new OptimizeCssAssetsPlugin(),
            // new UglifyJsPlugin(),
            // new TerserPlugin({
            //     terserOptions: {
            //         compress: {
            //             drop_console: true,
            //             drop_debugger: true,
            //             // keep_classnames: false,
            //             // keep_fnames:false,
            //         },
            //         mangle: {
            //             eval: true,
            //             keep_classnames: false,
            //             keep_fnames:false,
            //             reserved: [`once`, `passive`, `capture`, `user`, `msg`],
            //             toplevel: true,
            //             properties: {
            //                 reserved: [`once`, `passive`, `capture`, `user`, `msg`],
            //             }
            //         },
            //     }
            // }),
            new TerserPlugin({
                terserOptions: {
                    compress: {
                        drop_console: true,
                        drop_debugger: true,
                    }
                }}
            ),
        ]
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                use: {
                    loader: `html-loader`,
                    options: {
                        minimize: true,
                        removeComments: true,
                        collapseWhitespace: true,
                        root: path.resolve(__dirname),
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader, // pulls css back out of js and writes it to discrete css files
                    `css-loader`, // injects css into js
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader, // pulls css back out of js and writes it to discrete css files
                    `css-loader`, // injects css into js
                    // `sass-loader`, // compiles sass to css
                    {
                        loader: `sass-loader`,// compiles Sass to CSS
                        options: {
                            includePaths: [
                                `./node_modules/bourbon/app/assets/stylesheets`,
                            ]
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: `[name]-[contentHash].css`
        })
    ]
});