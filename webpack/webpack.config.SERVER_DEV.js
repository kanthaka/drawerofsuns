// const path = require(`path`);
const main = require(`./webpack.config.SERVER.js`);
const merge = require(`webpack-merge`);
const TerserPlugin = require(`terser-webpack-plugin`);
const nodeExternals = require('webpack-node-externals');

module.exports = merge(main, {
    mode: `production`,
    devtool: `none`,
    target: `node`,
    externals: [nodeExternals()],
    output: {
        path: `${process.env.PWD}/dev`,//path.resolve(__dirname, `build`),//process.env.PWD,//(path.resolve(__dirname(module.parent.filename))),
        filename: `[name].js`
    },
    optimization: {
        minimizer: [
            new TerserPlugin(),
        ]
    }
});