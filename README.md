# DRAWER OF SUNS

This is a tool used to generate posts for the (now mostly defunct) twitter pseudo-bot at [@sunSketches (twitter)](https://twitter.com/sunSketches).

A live version of this app should be available here: [drawerOfSuns (heroku)](https://drawerofsuns.herokuapp.com/).

The intent is to generate semi-random images via settings over which the user has only partial control.  There is no un-do feature, and while it is technically possible to give specific values to certain settings (by setting both the upper and lower randomization limits for that setting to same value), the tool is not designed in a way that would make it easy for the user to set all values in this manner.

### NOTES:

- This is a re-build of the earlier "manySuns" app, which was built without intention of it ever being used outside of the dev environment. 

- The "randomize all" feature (found under "general controls") is very un-opinionated, which means that it often gives un-usable results (there is no mechanism keeping it from randomizing all settings to extreme values) ... use it with care.