import React from 'react';
import Slider, { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';
import "./styles/main.scss";

function ControlItemSlot(props: {children?: any; title: string}): JSX.Element {
    return (
        <div className="control__item__container">
            <p className="control__item__title">{props.title}</p>
            <hr className="control__item__hr" />
            <div className="control__item__body">
                {props.children}
            </div>
        </div>
    );
}

function Control_SingleSlider(props: {title: string; sliderFunction: Function; buttonFunction_rndm: Function;  buttonFunction_calc: Function; min: number; max: number; valueSource: number}): JSX.Element {
    return (
        <ControlItemSlot title={props.title}> 
            <div className="control__item__sliderwithbutton">
                <Slider
                    style={{gridArea: `slider`, width: `80%`, alignSelf: `center`, justifySelf: `start`}}
                    /*onAfterChange*/onChange={props.sliderFunction}
                    min={props.min}
                    max={props.max}
                    value={props.valueSource}
                    // defaultValue={props.defaultValue}
                    /*allowCross={false}*/
                    pushable={0}
                    handleStyle={[{backgroundColor: `#224477`}, {backgroundColor: `#224477`}]}
                    trackStyle={[{backgroundColor: `#4488ee`}]}
                    railStyle={{backgroundColor: `#777777`}}
                />
                <button
                    className="control__item__sidebuton --rndm"
                    onClick={(): void => {props.buttonFunction_rndm();}}

                    // onClick={(event: React.MouseEvent<HTMLElement>) => {props.buttonFunction_rndm();}}
                >rndm</button>
                <button
                    className="control__item__sidebuton --calc"
                    onClick={(): void => {props.buttonFunction_calc();}}
                    // onClick={(event: React.MouseEvent<HTMLElement>) => {props.buttonFunction_calc();}}
                >calc</button>
            </div>
        </ControlItemSlot>
    );
}

function Control_SingleRange(props: {title: string; sliderFunction: Function; buttonFunction_rndm: Function; buttonFunction_calc: Function; min: number; max: number; valueSource: number[]}): JSX.Element {
    return (
        <ControlItemSlot title={props.title}> 
            <div className="control__item__sliderwithbutton">
                <Range
                    style={{gridArea: `slider`, width: `80%`, alignSelf: `center`, justifySelf: `start`}}
                    /*onAfterChange*/onChange={props.sliderFunction}
                    min={props.min}
                    max={props.max}
                    value={[props.valueSource[0], props.valueSource[1]]}
                    /*allowCross={false}*/
                    pushable={0}
                    handleStyle={[{backgroundColor: `#224477`}, {backgroundColor: `#224477`}]}
                    trackStyle={[{backgroundColor: `#4488ee`}]}
                    railStyle={{backgroundColor: `#777777`}}
                />
                <button
                    className="control__item__sidebuton --rndm"
                    onClick={(): void => {props.buttonFunction_rndm();}}
                    // onClick={(event: React.MouseEvent<HTMLElement>) => {props.buttonFunction_rndm();}}
                >rndm</button>
                <button
                    className="control__item__sidebuton --calc"
                    onClick={(): void => {props.buttonFunction_calc();}}
                    // onClick={(event: React.MouseEvent<HTMLElement>) => {props.buttonFunction_calc();}}
                >calc</button>
            </div>
        </ControlItemSlot>
    );
}

function Control_PairedRange(props: {title: string; sliderFunctionA: Function; sliderFunctionB: Function;  buttonFunction_rndm: Function; buttonFunction_calc: Function; minmaxA: number[]; minmaxB: number[]; valueSourceA: number[]; valueSourceB: number[]}): JSX.Element {
    return (
        <ControlItemSlot title="center point limits"> 
            <div className="control__item__sliderwithbutton">
                <Range
                    style={{gridArea: `slider`, width: `80%`, alignSelf: `start`, justifySelf: `start`}}
                    onChange={props.sliderFunctionA}
                    min={props.minmaxA[0]}
                    max={props.minmaxA[1]}
                    value={[props.valueSourceA[0], props.valueSourceA[1]]}
                    /*allowCross={false}*/
                    pushable={0}
                    handleStyle={[{backgroundColor: `#224477`}, {backgroundColor: `#224477`}]}
                    trackStyle={[{backgroundColor: `#4488ee`}]}
                    railStyle={{backgroundColor: `#777777`}}
                />
                <Range
                    style={{gridArea: `slider`, width: `80%`, alignSelf: `end`, justifySelf: `start`}}
                    onChange={props.sliderFunctionB}
                    min={props.minmaxB[0]}
                    max={props.minmaxB[1]}
                    value={[props.valueSourceB[0], props.valueSourceB[1]]}
                    /*allowCross={false}*/
                    pushable={0}
                    handleStyle={[{backgroundColor: `#224477`}, {backgroundColor: `#224477`}]}
                    trackStyle={[{backgroundColor: `#4488ee`}]}
                    railStyle={{backgroundColor: `#777777`}}
                />
                <button
                    className="control__item__sidebuton --rndm"
                    onClick={(): void => {props.buttonFunction_rndm();}}
                >rndm</button>
                <button
                    className="control__item__sidebuton --calc"
                    onClick={(): void => {props.buttonFunction_calc();}}
                >calc</button>
            </div>
        </ControlItemSlot>
    );
}

function Control_ColorRanges(props: {title: string; sliderFunction_H: Function; sliderFunction_S: Function; sliderFunction_L: Function; sliderFunction_A: Function; buttonFunction_rndm: Function; buttonFunction_calc: Function; mins: {}; maxs: {}; valueSources: {H: number[]; S: number[]; L: number[]; A: number[]}}): JSX.Element {
    return(
        <ControlItemSlot title={props.title}>
            <div className="control__item__sliderwithbutton--hsla">
                <Range
                    style={{gridArea: `1 / 1 / 2 / 2`, width: `80%`, alignSelf: `center`, justifySelf: `start`}}
                    onChange={props.sliderFunction_H}
                    min={props.mins[`H`][0]}
                    max={props.maxs[`H`][1]}
                    value={[props.valueSources[`H`][0], props.valueSources[`H`][1]]}
                    pushable={0}
                    handleStyle={[{backgroundColor: `#224477`}, {backgroundColor: `#224477`}]}
                    trackStyle={[{backgroundColor: `#4488ee`}]}
                    railStyle={{backgroundColor: `#777777`}}
                />
                <Range
                    style={{gridArea: `2 / 1 / 3 / 2`, width: `80%`, alignSelf: `center`, justifySelf: `start`}}
                    onChange={props.sliderFunction_S}
                    min={props.mins[`S`][0]}
                    max={props.maxs[`S`][1]}
                    value={[props.valueSources[`S`][0], props.valueSources[`S`][1]]}
                    pushable={0}
                    handleStyle={[{backgroundColor: `#224477`}, {backgroundColor: `#224477`}]}
                    trackStyle={[{backgroundColor: `#4488ee`}]}
                    railStyle={{backgroundColor: `#777777`}}
                />
                <Range
                    style={{gridArea: `3 / 1 / 4 / 2`, width: `80%`, alignSelf: `center`, justifySelf: `start`}}
                    onChange={props.sliderFunction_L}
                    min={props.mins[`L`][0]}
                    max={props.maxs[`L`][1]}
                    value={[props.valueSources[`L`][0], props.valueSources[`L`][1]]}
                    pushable={0}
                    handleStyle={[{backgroundColor: `#224477`}, {backgroundColor: `#224477`}]}
                    trackStyle={[{backgroundColor: `#4488ee`}]}
                    railStyle={{backgroundColor: `#777777`}}
                />
                <Range
                    style={{gridArea: `4 / 1 / 5 / 2`, width: `80%`, alignSelf: `center`, justifySelf: `start`}}
                    onChange={props.sliderFunction_A}
                    min={props.mins[`A`][0]}
                    max={props.maxs[`A`][1]}
                    value={[props.valueSources[`A`][0], props.valueSources[`A`][1]]}
                    pushable={0}
                    handleStyle={[{backgroundColor: `#224477`}, {backgroundColor: `#224477`}]}
                    trackStyle={[{backgroundColor: `#4488ee`}]}
                    railStyle={{backgroundColor: `#777777`}}
                />
                <button
                    className="control__item__sidebuton --rndm"
                    style={{gridArea: `1 / 2 / 5 / 3`}}
                    onClick={(): void => {props.buttonFunction_rndm();}}
                    // onClick={(event: React.MouseEvent<HTMLElement>) => {props.buttonFunction_rndm();}}
                >rndm</button>
                <button
                    className="control__item__sidebuton --calc"
                    style={{gridArea: `1 / 3 / 5 / 4`}}
                    onClick={(): void => {props.buttonFunction_calc();}}
                    // onClick={(event: React.MouseEvent<HTMLElement>) => {props.buttonFunction_calc();}}
                >calc</button>
            </div>
        </ControlItemSlot>
    );
}

export { ControlItemSlot, Control_SingleSlider, Control_SingleRange, Control_PairedRange, Control_ColorRanges };