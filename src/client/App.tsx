import React, { useReducer, useCallback } from 'react';
import ReactDOM from 'react-dom';
import init, { raysPerSideList, HslaLimitsInterface, DrawingLimitsInterface, SunSettingsInterface, RaySettingsInterface, strokeWidthList } from './init';
import { Display } from './Display';
import { Control } from './Control';
import './styles/main.scss';
import './favicon.ico';
import { Random } from "random-js";
const random = new Random();

interface AppContextInterface {
    drawingSettings: SunSettingsInterface[] | undefined;
    dispatchToDrawingSettings: Function;
    drawingBG: {A: string; B: string; points?: {x1: number; y1: number; x2: number; y2: number}};
    dispatchToDrawingBG: Function;
}

interface AppLimitsContextInterface {
    drawingLimits: DrawingLimitsInterface | undefined;
    dispatchToDrawingLimits: Function;
    drawingBGLimits: {A: HslaLimitsInterface; B: HslaLimitsInterface} | undefined;
    dispatchToDrawingBGLimits: Function;
}

export const AppContext = React.createContext<AppContextInterface>({
    drawingSettings: undefined,
    dispatchToDrawingSettings: (): void => {},
    drawingBG: undefined,
    dispatchToDrawingBG: (): void => {},
});

export const AppLimitsContext = React.createContext<AppLimitsContextInterface>({
    drawingLimits: undefined,
    dispatchToDrawingLimits: (): void => {},
    drawingBGLimits: undefined,
    dispatchToDrawingBGLimits: (): void => {},
});

function App(): JSX.Element {

    const [drawingBGLimits, dispatchToDrawingBGLimits] = useReducer((state: {A: HslaLimitsInterface; A_MINMAX: HslaLimitsInterface; B: HslaLimitsInterface; B_MINMAX: HslaLimitsInterface}, action: {type: string; value?: any; pole: string; facet: string}): {A: HslaLimitsInterface; A_MINMAX: HslaLimitsInterface; B: HslaLimitsInterface; B_MINMAX: HslaLimitsInterface} => {
        let tempState = {...state};
        switch(action.type) {
            case `BGHslaALimits`:
                tempState[action.pole][`${action.facet}`] = init.sortIntegerArray([action.value[0], action.value[1]]);
                break;
            case `randomBGHslaLimits`:
                [`H`, `S`, `L`, `A`,].map((item, index): void => {
                    tempState[action.pole][item] = init.sortIntegerArray([random.integer(action.value[`${action.pole}_MINMAX`][item][0], action.value[`${action.pole}_MINMAX`][item][1]), random.integer(action.value[`${action.pole}_MINMAX`][item][0], action.value[`${action.pole}_MINMAX`][item][1])]);
                });
                break;
            default:
                console.log(`default case BG`);
                break;
        }
        return tempState;
    }, {
        A: {
            H: [0, 359],
            S: [0,100],
            L: [0,100],
            A: [100,100],
        },
        A_MINMAX: {
            H: [0, 359],
            S: [0,100],
            L: [0,100],
            A: [0,100],
        },
        B: {
            H: [0, 359],
            S: [0,100],
            L: [0,100],
            A: [0,100],
        },
        B_MINMAX: {
            H: [0, 359],
            S: [0,100],
            L: [0,100],
            A: [0,100],
        },
    });

    const [drawingBG, dispatchToDrawingBG] = useReducer((state: {A: string; B: string; points: {x1: number; y1: number; x2: number; y2: number}}, action: {type: string; value?: number; pole: string; facet: string}): {A: string; B: string} => {
        let tempState = {...state};
        switch(action.type) {
            case `setBGPoints`:
                tempState.points = {
                    x1: random.integer(-50, 150),
                    y1: random.integer(-50, 150),
                    x2: random.integer(-50, 150),
                    y2: random.integer(-50, 150),
                };
                break;
            case `setBGHsla`:
                tempState[action.pole] = init.getRandomHslaWithLimits(drawingBGLimits[action.pole]);
                break;
            default:
                console.log(`default case bg hsla`);
                break;
        }
        return tempState;
    }, init.buildDrawingBG(drawingBGLimits));

    const [drawingLimits, dispatchToDrawingLimits] = useReducer((state: DrawingLimitsInterface, action: {type: string; nameOfValueToSet?: string; nameOfValueToRandomize?: string; value: any; facet?: string}): DrawingLimitsInterface => {
        let tempState = {...state};
        switch(action.type) {
            case `setSingleValue`:
                tempState[action.nameOfValueToSet] = action.value;
                break;
            case `setPairedValues`:
                action.nameOfValueToSet.includes(`/`) === false ?
                    tempState[action.nameOfValueToSet] = init.sortIntegerArray([action.value[0], action.value[1]])
                    :
                    tempState[action.nameOfValueToSet.split(`/`)[0]][action.nameOfValueToSet.split(`/`)[1]] = init.sortIntegerArray([action.value[0], action.value[1]]);
                break;
            case `randomSunCountLimits`:
                tempState.sunCountLimits = random.integer(1, action.value[1]);
                break;
            case `randomCenterPointLimits`:
                tempState.centerPointLimitsX = init.sortIntegerArray([random.integer(action.value[0], action.value[1]), random.integer(action.value[0], action.value[1])]);
                tempState.centerPointLimitsY = init.sortIntegerArray([random.integer(action.value[2], action.value[3]), random.integer(action.value[2], action.value[3])]);
                break;
            case `randomizeSingleValue`:
                tempState[action.nameOfValueToRandomize] = random.integer(action.value[0], action.value[1]);
                break;
            case `randomizePairedValues`:
                tempState[action.nameOfValueToRandomize] = init.sortIntegerArray([random.integer(action.value[0], action.value[1]), random.integer(action.value[0], action.value[1])]);
                break;
            case `randomizeHslaValues`:
                [`H`, `S`, `L`, `A`,].map((item, index): void => {
                    tempState[action.nameOfValueToRandomize][item] = init.sortIntegerArray([random.integer(action.value[item][0], action.value[item][1]), random.integer(action.value[item][0], action.value[item][1])]);
                });
                break;
            default:
                console.log(`DEFAULT CASE (limits context)`);
                break;
        }
        return tempState;
    }, {
        sunCountLimits: 2,//[2,2],
        sunCountLimits_MINMAX: [0, 8],

        centerPointLimitsX: [0, 100],
        centerPointLimitsX_MINMAX: [-20, 120],

        centerPointLimitsY: [0, 100],
        centerPointLimitsY_MINMAX: [-20, 120],

        orbSizeLimits: [10, 20],
        orbSizeLimits_MINMAX: [0, 48],

        orbHslaLimits: {H: [0, 360], S: [25, 75], L: [20, 80], A: [100, 100]},
        orbHslaLimits_MINMAX: {H: [0, 360], S: [0, 100], L: [0, 100], A: [0, 100]},

        orbStrokeWidthLimits: [0, Math.ceil(strokeWidthList.length/2)],
        orbStrokeWidthLimits_MINMAX: [0, strokeWidthList.length-1],

        orbStrokeHslaLimits: {H: [0, 360], S: [25, 75], L: [20, 80], A: [100, 100]},
        orbStrokeHslaLimits_MINMAX: {H: [0, 360], S: [0, 100], L: [0, 100], A: [0, 100]},

        rayUniformityChanceLimits: 50,
        rayUniformityChanceLimits_MINMAX: [0, 100],

        raysPerSideLimits: [1, 2],
        raysPerSideLimits_MINMAX: [0, raysPerSideList.length-1],

        rayPointOffsetModifierLimits: [10, 20],
        rayPointOffsetModifierLimits_MINMAX: [5, 45],

        rayFillHslaALimits: {H: [0, 360], S: [25, 75], L: [20, 80], A: [75, 100]},
        rayFillHslaALimits_MINMAX: {H: [0, 360], S: [0, 100], L: [0, 100], A: [0, 100]},
        rayFillHslaBLimits: {H: [0, 360], S: [25, 75], L: [20, 80], A: [75, 100]},
        rayFillHslaBLimits_MINMAX: {H: [0, 360], S: [0, 100], L: [0, 100], A: [0, 100]},

        rayGradientPointsLimits_MINMAX: [-100, 200],

        rayStrokeWidthLimits: [0, Math.ceil(strokeWidthList.length/2)],
        rayStrokeWidthLimits_MINMAX: [0, strokeWidthList.length-1],

        rayStrokeHslaLimits: {H: [0, 360], S: [25, 75], L: [20, 80], A: [100, 100]},
        rayStrokeHslaLimits_MINMAX: {H: [0, 360], S: [0, 100], L: [0, 100], A: [0, 100]},

        raySetCountLimits: [1, 3],
        raySetCountLimits_MINMAX: [1, 6],

        rayAnimateChanceLimits: 50,
        rayAnimateChanceLimits_MINMAX: [0, 100],
    });

    function changeSunCount(tempState: SunSettingsInterface[], rndm: boolean = false): SunSettingsInterface[] {
        let newSunCount = drawingLimits.sunCountLimits;
        if (newSunCount < tempState.length) {
            tempState = tempState.slice(0, newSunCount);
        } else if (newSunCount > tempState.length) {
            let additionalSuns: SunSettingsInterface[] = [];
            for (let i = 0; i < newSunCount - tempState.length; i++) {
                additionalSuns[i] = init.buildSunSettings(drawingLimits);
            }
            tempState = [
                ...tempState,
                ...additionalSuns,
            ];
        }
        return tempState;
    }

    function changeRaySetCount(tempState: SunSettingsInterface[]): SunSettingsInterface[] {
        let newRaySetCount = random.integer(drawingLimits.raySetCountLimits[0], drawingLimits.raySetCountLimits[1]);
        let additionalRaySets: RaySettingsInterface[] = [];
        for(let i=0;i<tempState.length;i++) {
            if (newRaySetCount < tempState[i].rays.length) {
                tempState[i].rays = tempState[i].rays.slice(0, newRaySetCount);
            } else if (newRaySetCount > tempState[i].rays.length) {
                for (let j = 0; j < newRaySetCount - tempState[i].rays.length; j++) {
                    additionalRaySets[i] = init.buildRaySettings(drawingLimits);
                }
                tempState[i].rays = [
                    ...tempState[i].rays,
                    ...additionalRaySets,
                ];
            }
        }
        return tempState;
    }

    function changeRaysPerSide(tempState: SunSettingsInterface[]): SunSettingsInterface[] {
        for(let i = 0; i < tempState.length; i++) {
            for (let j = 0; j < tempState[i].rays.length; j++) {
                tempState[i].rays[j].raysPerSide = raysPerSideList[random.integer(drawingLimits.raysPerSideLimits[0], drawingLimits.raysPerSideLimits[1])];
                tempState[i].rays[j].rayPoints = init.buildRayPoints(tempState[i].rays[j].rayPoints, tempState[i].rays[j].rayUniformity, tempState[i].rays[j].rayPointGranularity, tempState[i].rays[j].raysPerSide);
            }
        }
        return tempState;
    }

    const [drawingSettings, dispatchToDrawingSettings] = useReducer((state: SunSettingsInterface[], action: {type: string; value?: number}): SunSettingsInterface[] => {
        let tempState = [...state];
        let previousRayUniformity: boolean;

        switch (action.type) {

            case `redraw`:
                tempState = init.buildDrawingSettings(drawingLimits);
                break;

            case `shuffle`:
                while (tempState.length > 1 && (tempState[0].id == state[0].id || tempState[tempState.length-1].id == state[tempState.length-1].id)) {
                    random.shuffle(tempState);
                }
                break;

            case `setSunCount`:
                tempState = changeSunCount(tempState);
                break;

            case `setCenterPoints`:
                for(let i=0;i<tempState.length;i++) {
                    tempState[i].centerPoint=`${random.integer(drawingLimits.centerPointLimitsX[0], drawingLimits.centerPointLimitsX[1])} ${random.integer(drawingLimits.centerPointLimitsY[0], drawingLimits.centerPointLimitsY[1])}`;
                }
                break;

            case `setOrbSize`:
                for(let i=0;i<tempState.length;i++) {
                    tempState[i].orbSize=random.integer(drawingLimits.orbSizeLimits[0], drawingLimits.orbSizeLimits[1]);
                }
                break;

            case `setOrbStrokeWidth`:
                for(let i=0;i<tempState.length;i++) {
                    tempState[i].orbStrokeWidth = strokeWidthList[random.integer(drawingLimits.orbStrokeWidthLimits[0], drawingLimits.orbStrokeWidthLimits[1])]/100;
                }
                break;

            case `setRayStrokeWidth`:
                for(let i=0;i<tempState.length;i++) {
                    for (let j=0;j<tempState[i].rays.length;j++) {
                        tempState[i].rays[j].rayStrokeWidth = strokeWidthList[random.integer(drawingLimits.rayStrokeWidthLimits[0], drawingLimits.rayStrokeWidthLimits[1])]/100;
                    }
                }
                break;

            case `setOrbHsla`:
                for(let i=0;i<tempState.length;i++) {
                    tempState[i].orbColor = init.getRandomHslaWithLimits(drawingLimits.orbHslaLimits);
                }
                break;

            case `setOrbStrokeHsla`:
                for(let i=0;i<tempState.length;i++) {
                    tempState[i].orbStrokeHsla = init.getRandomHslaWithLimits(drawingLimits.orbStrokeHslaLimits);
                }
                break;

            case `setRayStrokeHsla`:
                for(let i=0;i<tempState.length;i++) {
                    for (let j=0;j<tempState[i].rays.length;j++) {
                        tempState[i].rays[j].rayStrokeHsla = init.getRandomHslaWithLimits(drawingLimits.rayStrokeHslaLimits);
                    }
                }
                break;

            case `setRayFillHsla`:
                for(let i=0;i<tempState.length;i++) {
                    for (let j=0;j<tempState[i].rays.length;j++) {
                        tempState[i].rays[j][`rayFillColor${action.value}`] = init.getRandomHslaWithLimits(drawingLimits[`rayFillHsla${action.value}Limits`]);
                    }
                }
                break;

            case `setRayUniformityChance`:
                for(let i=0;i<tempState.length;i++) {
                    for (let j=0;j<tempState[i].rays.length;j++) {
                        previousRayUniformity = tempState[i].rays[j].rayUniformity;
                        tempState[i].rays[j].rayUniformity = random.bool(drawingLimits.rayUniformityChanceLimits/100);
                        tempState[i].rays[j].rayPoints = init.buildRayPoints(tempState[i].rays[j].rayPoints, tempState[i].rays[j].rayUniformity, tempState[i].rays[j].rayPointGranularity, tempState[i].rays[j].raysPerSide, previousRayUniformity);
                    }
                }
                break;

            case `setRaysPerSide`:
                tempState = changeRaysPerSide(tempState);
                break;

            case `setRayPointOffsetModifier`:
                for(let i=0;i<tempState.length;i++) {
                    for (let j=0;j<tempState[i].rays.length;j++) {
                        tempState[i].rays[j].rayPointOffsetModifier = random.integer(drawingLimits.rayPointOffsetModifierLimits[0], drawingLimits.rayPointOffsetModifierLimits[1]);
                        tempState[i].rays[j].rayPoints = init.buildRayPoints(tempState[i].rays[j].rayPoints, tempState[i].rays[j].rayUniformity, tempState[i].rays[j].rayPointGranularity, tempState[i].rays[j].raysPerSide);
                    }
                }
                break;

            case `setRayGradientPoints`:
                for(let i=0;i<tempState.length;i++) {
                    for (let j=0;j<tempState[i].rays.length;j++) {
                        tempState[i].rays[j].rayGradientPoints = {
                            x1: random.integer(drawingLimits.rayGradientPointsLimits_MINMAX[0], drawingLimits.rayGradientPointsLimits_MINMAX[1]),
                            y1: random.integer(drawingLimits.rayGradientPointsLimits_MINMAX[0], drawingLimits.rayGradientPointsLimits_MINMAX[1]),
                            x2: random.integer(drawingLimits.rayGradientPointsLimits_MINMAX[0], drawingLimits.rayGradientPointsLimits_MINMAX[1]),
                            y2: random.integer(drawingLimits.rayGradientPointsLimits_MINMAX[0], drawingLimits.rayGradientPointsLimits_MINMAX[1]),
                        };
                    }
                }
                break;

            case `setRaySetCount`:
                tempState = changeRaySetCount(tempState);
                break;

            case `setRayAnimateChance`:
                for (let i=0;i<tempState.length;i++) {
                    for (let j=0;j<tempState[i].rays.length;j++) {
                        tempState[i].rays[j].baseRotation = random.integer(0 , 360);
                        tempState[i].rays[j].rayRotateDirection = random.pick([
                            [tempState[i].rays[j].baseRotation, tempState[i].rays[j].baseRotation+360],
                            [tempState[i].rays[j].baseRotation, tempState[i].rays[j].baseRotation-360],
                        ]);
                        tempState[i].rays[j].rayAnimate = random.bool(drawingLimits.rayAnimateChanceLimits/100);
                    }
                }
                break;

            default: 
                console.log(`DEFAULT CASE (settings context)`);
                tempState = state;
                break;//return state;
        }
        return tempState;
    }, init.buildDrawingSettings(drawingLimits));

    return (
        <AppContext.Provider value={{drawingSettings: drawingSettings, dispatchToDrawingSettings: dispatchToDrawingSettings, drawingBG: drawingBG, dispatchToDrawingBG: dispatchToDrawingBG}}>
            {console.log(`app render`)}
            {/* {console.log(drawingBG)} */}
            <div className="app__container">
                <Display />
                <AppLimitsContext.Provider value={{drawingLimits: drawingLimits, dispatchToDrawingLimits: dispatchToDrawingLimits, drawingBGLimits: drawingBGLimits, dispatchToDrawingBGLimits: dispatchToDrawingBGLimits}}>
                    <Control />
                </AppLimitsContext.Provider>
            </div>
        </AppContext.Provider>
    );
}

ReactDOM.render(
    <App />,
    document.querySelector(`#root__container`),
);