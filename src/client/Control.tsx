import React, { useState, useContext } from 'react';
import { AppContext, AppLimitsContext } from './App';
import { ControlItemSlot, Control_SingleSlider, Control_SingleRange, Control_PairedRange, Control_ColorRanges } from './Control_definitions';
import 'rc-slider/assets/index.css';
import "./styles/main.scss";

export function Control(): JSX.Element {
    return (
        <div className="control__container">
            {console.log(`control render`)}
            <ControlBoard />
        </div>
    );
}

function ControlBoard(): JSX.Element {

    const [show, setShow]: [boolean, Function] = useState(true);

    return (
        <div className="control__board">
            {show === true
            ?
            <>
                <ControlBlock_GeneralControls id="controlBlock_generalControls" />
                <ControlBlock_BackgroundConfig id="controlBlock_backgroundConfig" />
                <ControlBlock_DrawingConfig id="controlBlock_drawingConfig" />
                <ControlBlock_OrbConfig id="controlBlock_orbConfig" />
                <ControlBlock_OrbFillAndStroke id="controlBlock_orbFillAndStroke" />
                <ControlBlock_RayConfig id="controlBlock_rayConfig" />
                <ControlBlock_RayFillAndStroke id="controlBlock_rayFillAndStroke" />
            </>
            :
            <></>}
        </div>
    );
}

function ControlBlock_GeneralControls(props: {id: string}): JSX.Element {

    const appContext = useContext(AppContext);
    const dispatchToDrawingSettings = appContext.dispatchToDrawingSettings;
    const dispatchToDrawingBG = appContext.dispatchToDrawingBG;

    const [active, setActive]: [boolean, Function] = useState(false);
    function toggleActive(event: Event | undefined): void {
        active === false
            ?
            setActive(true)
            :
            setActive(false);
    }

    return (
        <>
            {console.log(`${props.id} render`)}
            <div className="control__block__title" onClick={(): void => {toggleActive(event);}}>
                <h1 className="control__block__title__text">general controls</h1>
            </div>
            <div id={props.id} className="control__block__body" style={active === false ? {maxHeight: `0`} : {maxHeight: `${(document.querySelector(`#${props.id}`) as Element).scrollHeight}px`}}> 

                <ControlItemSlot title="rebuild all"> 
                    <button className="control__item__button" onClick={(): void => {
                        dispatchToDrawingSettings({type:`redraw`});
                        dispatchToDrawingBG({type: `setBGHsla`, pole: `A`});
                        dispatchToDrawingBG({type: `setBGHsla`, pole: `B`});
                        dispatchToDrawingBG({type: `setBGPoints`});
                    }}>rebuild drawing</button>
                </ControlItemSlot>
                <ControlItemSlot title="randomize all"> 
                    <button className="control__item__button" onClick={(event): void => {
                        Array.from(document.querySelectorAll(`.--rndm`)).map((item, idnex): void => {
                            (item as HTMLButtonElement).click();
                        });
                    }}
                    >randomize all</button>
                </ControlItemSlot>

            </div>
        </>
    );
}

function ControlBlock_BackgroundConfig(props: {id: string}): JSX.Element {

    const appLimitsContext = useContext(AppLimitsContext);
    const drawingBGLimits = appLimitsContext.drawingBGLimits;
    const dispatchToDrawingBGLimitsContext = appLimitsContext.dispatchToDrawingBGLimits;

    const appContext = useContext(AppContext);
    const dispatchToDrawingBG = appContext.dispatchToDrawingBG;

    const [active, setActive]: [boolean, Function] = useState(false);
    function toggleActive(event: Event | undefined): void {
        active === false
            ?
            setActive(true)
            :
            setActive(false);
    }

    return (
        <>
            {console.log(`${props.id} render`)}
            <div className="control__block__title" onClick={(): void => {toggleActive(event);}}>
                <h1 className="control__block__title__text">background config</h1>
            </div>
            <div id={props.id} className="control__block__body" style={active === false ? {maxHeight: `0`} : {maxHeight: `${(document.querySelector(`#${props.id}`) as Element).scrollHeight}px`}}> 


                <Control_ColorRanges
                    title="bg hsla limits A"
                    sliderFunction_H={(value: number[]): void => {dispatchToDrawingBGLimitsContext({type: `BGHslaALimits`, value: [value[0],value[1]], pole: `A`, facet: `H`});}}
                    sliderFunction_S={(value: number[]): void => {dispatchToDrawingBGLimitsContext({type: `BGHslaALimits`, value: [value[0],value[1]], pole: `A`, facet: `S`});}}
                    sliderFunction_L={(value: number[]): void => {dispatchToDrawingBGLimitsContext({type: `BGHslaALimits`, value: [value[0],value[1]], pole: `A`, facet: `L`});}}
                    sliderFunction_A={(value: number[]): void => {dispatchToDrawingBGLimitsContext({type: `BGHslaALimits`, value: [value[0],value[1]], pole: `A`, facet: `A`});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingBGLimitsContext({type:`randomBGHslaLimits`, pole: `A`, value: drawingBGLimits});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingBG({type:`setBGHsla`, pole: `A`});}}
                    mins={drawingBGLimits[`A_MINMAX`]}
                    maxs={drawingBGLimits[`A_MINMAX`]}
                    valueSources={drawingBGLimits.A}
                ></Control_ColorRanges>

                <Control_ColorRanges
                    title="bg hsla limits B"
                    sliderFunction_H={(value: number[]): void => {dispatchToDrawingBGLimitsContext({type: `BGHslaALimits`, value: [value[0],value[1]], pole: `B`, facet: `H`});}}
                    sliderFunction_S={(value: number[]): void => {dispatchToDrawingBGLimitsContext({type: `BGHslaALimits`, value: [value[0],value[1]], pole: `B`, facet: `S`});}}
                    sliderFunction_L={(value: number[]): void => {dispatchToDrawingBGLimitsContext({type: `BGHslaALimits`, value: [value[0],value[1]], pole: `B`, facet: `L`});}}
                    sliderFunction_A={(value: number[]): void => {dispatchToDrawingBGLimitsContext({type: `BGHslaALimits`, value: [value[0],value[1]], pole: `B`, facet: `A`});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingBGLimitsContext({type:`randomBGHslaLimits`, pole: `B`, value: drawingBGLimits});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingBG({type:`setBGHsla`, pole: `B`});}}
                    mins={drawingBGLimits[`B_MINMAX`]}
                    maxs={drawingBGLimits[`B_MINMAX`]}
                    valueSources={drawingBGLimits.B}
                ></Control_ColorRanges>

                <ControlItemSlot title="bg gradient points"> 
                    <button className="control__item__button" onClick={(event): void => {
                        dispatchToDrawingBG({type: `setBGPoints`});
                    }}
                    >reset points</button>
                </ControlItemSlot>

            </div>
        </>
    );
}

function ControlBlock_DrawingConfig(props: {id: string}): JSX.Element {

    const appContext = useContext(AppContext);
    const dispatchToDrawingSettings = appContext.dispatchToDrawingSettings;

    const appLimitsContext = useContext(AppLimitsContext);
    const drawingLimits = appLimitsContext.drawingLimits;
    const dispatchToDrawingLimitsContext = appLimitsContext.dispatchToDrawingLimits;

    const [active, setActive]: [boolean, Function] = useState(false);
    function toggleActive(event: Event | undefined): void {
        active === false
            ?
            setActive(true)
            :
            setActive(false);
    }

    return (
        <>
            {console.log(`${props.id} render`)}
            <div className="control__block__title" onClick={(): void => {toggleActive(event);}}>
                <h1 className="control__block__title__text">drawing config</h1>
            </div>
            <div id={props.id} className="control__block__body" style={active === false ? {maxHeight: `0`} : {maxHeight: `${(document.querySelector(`#${props.id}`) as Element).scrollHeight}px`}}> 

                <Control_SingleSlider
                    title="number of suns"
                    sliderFunction={(value: number): void => {dispatchToDrawingLimitsContext({type: `setSingleValue`, nameOfValueToSet:`sunCountLimits`, value: value});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type:`randomSunCountLimits`, value: drawingLimits.sunCountLimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setSunCount`});}}
                    min={drawingLimits.sunCountLimits_MINMAX[0]}
                    max={drawingLimits.sunCountLimits_MINMAX[1]}
                    valueSource={drawingLimits.sunCountLimits}
                ></Control_SingleSlider>
                <ControlItemSlot title="shuffle suns"> 
                    <button className="control__item__button" onClick={(event): void => {
                        dispatchToDrawingSettings({type: `shuffle`});
                    }}
                    >shuffle suns</button>
                </ControlItemSlot>

            </div>
        </>
    );
}

function ControlBlock_OrbConfig(props: {id: string}): JSX.Element {

    const appLimitsContext = useContext(AppLimitsContext);
    const drawingLimits = appLimitsContext.drawingLimits;
    const dispatchToDrawingLimitsContext = appLimitsContext.dispatchToDrawingLimits;

    const appContext = useContext(AppContext);
    const dispatchToDrawingSettings = appContext.dispatchToDrawingSettings;

    const [active, setActive]: [boolean, Function] = useState(false);
    function toggleActive(event: Event | undefined): void {
        active === false
            ?
            setActive(true)
            :
            setActive(false);
    }

    return (
        <>
            {console.log(`${props.id} render`)}
            <div className="control__block__title" onClick={(): void => {toggleActive(event);}}>
                <h1 className="control__block__title__text">orb config</h1>
            </div>
            <div id={props.id} className="control__block__body" style={active === false ? {maxHeight: `0`} : {maxHeight: `${(document.querySelector(`#${props.id}`) as Element).scrollHeight}px`}}> 

                <Control_PairedRange
                    title="center point limits"
                    sliderFunctionA={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `centerPointLimitsX`, value: [value[0],value[1]]});}}
                    sliderFunctionB={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `centerPointLimitsY`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type:`randomCenterPointLimits`, value: [drawingLimits.centerPointLimitsX_MINMAX[0], drawingLimits.centerPointLimitsX_MINMAX[1], drawingLimits.centerPointLimitsY_MINMAX[0], drawingLimits.centerPointLimitsY_MINMAX[1]]});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setCenterPoints`});}}
                    minmaxA={[drawingLimits.centerPointLimitsX_MINMAX[0], drawingLimits.centerPointLimitsX_MINMAX[1]]}
                    minmaxB={[drawingLimits.centerPointLimitsY_MINMAX[0], drawingLimits.centerPointLimitsY_MINMAX[1]]}
                    valueSourceA={[drawingLimits.centerPointLimitsX[0], drawingLimits.centerPointLimitsX[1]]}
                    valueSourceB={[drawingLimits.centerPointLimitsY[0], drawingLimits.centerPointLimitsY[1]]}
                ></Control_PairedRange>

                <Control_SingleRange
                    title="orb size limits"
                    sliderFunction={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `orbSizeLimits`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type:`randomizePairedValues`, nameOfValueToRandomize: `orbSizeLimits`, value: drawingLimits.orbSizeLimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setOrbSize`});}}
                    min={drawingLimits.orbSizeLimits_MINMAX[0]}
                    max={drawingLimits.orbSizeLimits_MINMAX[1]}
                    valueSource={[drawingLimits.orbSizeLimits[0], drawingLimits.orbSizeLimits[1]]}
                ></Control_SingleRange>

            </div>
        </>
    );
}

function ControlBlock_OrbFillAndStroke(props: {id: string}): JSX.Element {

    const appLimitsContext = useContext(AppLimitsContext);
    const drawingLimits = appLimitsContext.drawingLimits;
    const dispatchToDrawingLimitsContext = appLimitsContext.dispatchToDrawingLimits;

    const appContext = useContext(AppContext);
    const dispatchToDrawingSettings = appContext.dispatchToDrawingSettings;

    const [active, setActive]: [boolean, Function] = useState(false);
    function toggleActive(event: Event | undefined): void {
        active === false
            ?
            setActive(true)
            :
            setActive(false);
    }

    return (
        <>
            {console.log(`${props.id} render`)}
            <div className="control__block__title" onClick={(): void => {toggleActive(event);}}>
                <h1 className="control__block__title__text">orb fill/stroke</h1>
            </div>
            <div id={props.id} className="control__block__body" style={active === false ? {maxHeight: `0`} : {maxHeight: `${(document.querySelector(`#${props.id}`) as Element).scrollHeight}px`}}> 

                <Control_ColorRanges
                    title="orb fill hsla limits"
                    sliderFunction_H={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `orbHslaLimits/H`, value: [value[0],value[1]]});}}
                    sliderFunction_S={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `orbHslaLimits/S`, value: [value[0],value[1]]});}}
                    sliderFunction_L={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `orbHslaLimits/L`, value: [value[0],value[1]]});}}
                    sliderFunction_A={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `orbHslaLimits/A`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type: `randomizeHslaValues`, nameOfValueToRandomize: `orbHslaLimits`, value: drawingLimits.orbHslaLimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setOrbHsla`});}}
                    mins={drawingLimits.orbHslaLimits_MINMAX}
                    maxs={drawingLimits.orbHslaLimits_MINMAX}
                    valueSources={drawingLimits.orbHslaLimits}
                ></Control_ColorRanges>

                <Control_SingleRange
                    title="orb stroke width limits"
                    sliderFunction={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `orbStrokeWidthLimits`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type:`randomizePairedValues`, nameOfValueToRandomize:`orbStrokeWidthLimits`, value: drawingLimits.orbStrokeWidthLimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setOrbStrokeWidth`});}}
                    min={drawingLimits.orbStrokeWidthLimits_MINMAX[0]}
                    max={drawingLimits.orbStrokeWidthLimits_MINMAX[1]}
                    valueSource={[drawingLimits.orbStrokeWidthLimits[0], drawingLimits.orbStrokeWidthLimits[1]]}
                ></Control_SingleRange>

                <Control_ColorRanges
                    title="orb stroke hsla limits"
                    sliderFunction_H={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `orbStrokeHslaLimits/H`, value: [value[0],value[1]]});}}
                    sliderFunction_S={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `orbStrokeHslaLimits/S`, value: [value[0],value[1]]});}}
                    sliderFunction_L={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `orbStrokeHslaLimits/L`, value: [value[0],value[1]]});}}
                    sliderFunction_A={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `orbStrokeHslaLimits/A`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type: `randomizeHslaValues`, nameOfValueToRandomize: `orbStrokeHslaLimits`, value: drawingLimits.orbStrokeHslaLimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setOrbStrokeHsla`});}}
                    mins={drawingLimits.orbStrokeHslaLimits_MINMAX}
                    maxs={drawingLimits.orbStrokeHslaLimits_MINMAX}
                    valueSources={drawingLimits.orbStrokeHslaLimits}
                ></Control_ColorRanges>

            </div>
        </>
    );
}

function ControlBlock_RayConfig(props: {id: string}): JSX.Element {

    const appLimitsContext = useContext(AppLimitsContext);
    const drawingLimits = appLimitsContext.drawingLimits;
    const dispatchToDrawingLimitsContext = appLimitsContext.dispatchToDrawingLimits;

    const appContext = useContext(AppContext);
    const dispatchToDrawingSettings = appContext.dispatchToDrawingSettings;

    const [active, setActive]: [boolean, Function] = useState(false);

    function toggleActive(event: Event | undefined): void {
        active === false
            ?
            setActive(true)
            :
            setActive(false);
    }

    return (
        <>
            {console.log(`controlblock render`)}
            <div className="control__block__title" onClick={(): void => {toggleActive(event);}}>
                <h1 className="control__block__title__text">ray config</h1>
            </div>
            <div id={props.id} className="control__block__body" style={active === false ? {maxHeight: `0`} : {/*backgroundColor: `blue`, height: `${7.5*6}rem`*/maxHeight: `${(document.querySelector(`#${props.id}`) as Element).scrollHeight}px`}}> 

                <Control_SingleSlider
                    title="chance for uniform rays"
                    sliderFunction={(value: number): void => {dispatchToDrawingLimitsContext({type: `setSingleValue`, nameOfValueToSet: `rayUniformityChanceLimits`, value: value});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type:`randomizeSingleValue`, nameOfValueToRandomize:`rayUniformityChanceLimits`, value: drawingLimits.rayUniformityChanceLimits_MINMAX});/*dispatchToDrawingSettings({type:`setSunCount`});*/}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setRayUniformityChance`});}}
                    min={drawingLimits.rayUniformityChanceLimits_MINMAX[0]}
                    max={drawingLimits.rayUniformityChanceLimits_MINMAX[1]}
                    valueSource={drawingLimits.rayUniformityChanceLimits}
                ></Control_SingleSlider>

                <Control_SingleRange
                    title="rays per side limits"
                    sliderFunction={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `raysPerSideLimits`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type:`randomizePairedValues`, nameOfValueToRandomize: `raysPerSideLimits`, value: drawingLimits.raysPerSideLimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setRaysPerSide`});}}

                    min={drawingLimits.raysPerSideLimits_MINMAX[0]}
                    max={drawingLimits.raysPerSideLimits_MINMAX[1]}
                    valueSource={[drawingLimits.raysPerSideLimits[0], drawingLimits.raysPerSideLimits[1]]}
                ></Control_SingleRange>

                <Control_SingleRange
                    title="rayset count limits"
                    sliderFunction={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `raySetCountLimits`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type: `randomizePairedValues`, nameOfValueToRandomize: `raySetCountLimits`, value: drawingLimits.sunCountLimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setRaySetCount`});}}
                    min={drawingLimits.raySetCountLimits_MINMAX[0]}
                    max={drawingLimits.raySetCountLimits_MINMAX[1]}
                    valueSource={[drawingLimits.raySetCountLimits[0], drawingLimits.raySetCountLimits[1]]}
                ></Control_SingleRange>

                <Control_SingleSlider
                    title="chance to animate"
                    sliderFunction={(value: number): void => {dispatchToDrawingLimitsContext({type: `setSingleValue`, nameOfValueToSet: `rayAnimateChanceLimits`, value: value});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type: `randomizeSingleValue`, nameOfValueToRandomize: `rayAnimateChanceLimits`, value: drawingLimits.rayAnimateChanceLimits_MINMAX});/*dispatchToDrawingSettings({type:`setSunCount`});*/}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setRayAnimateChance`});}}
                    min={drawingLimits.rayAnimateChanceLimits_MINMAX[0]}
                    max={drawingLimits.rayAnimateChanceLimits_MINMAX[1]}
                    valueSource={drawingLimits.rayAnimateChanceLimits}
                ></Control_SingleSlider>

                <Control_SingleRange
                    title="ray point offset modifier"
                    sliderFunction={(value: number): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayPointOffsetModifierLimits`, value: value});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type: `randomizePairedValues`, nameOfValueToRandomize: `rayPointOffsetModifierLimits`, value: drawingLimits.rayPointOffsetModifierLimits_MINMAX});/*dispatchToDrawingSettings({type:`setSunCount`});*/}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setRayPointOffsetModifier`});}}
                    min={drawingLimits.rayPointOffsetModifierLimits_MINMAX[0]}
                    max={drawingLimits.rayPointOffsetModifierLimits_MINMAX[1]}
                    valueSource={drawingLimits.rayPointOffsetModifierLimits}
                ></Control_SingleRange>

            </div>
        </>
    );
}

function ControlBlock_RayFillAndStroke(props: {id: string}): JSX.Element {

    const appLimitsContext = useContext(AppLimitsContext);
    const drawingLimits = appLimitsContext.drawingLimits;
    const dispatchToDrawingLimitsContext = appLimitsContext.dispatchToDrawingLimits;

    const appContext = useContext(AppContext);
    const dispatchToDrawingSettings = appContext.dispatchToDrawingSettings;

    const [active, setActive]: [boolean, Function] = useState(false);

    function toggleActive(event: Event | undefined): void {
        active === false
            ?
            setActive(true)
            :
            setActive(false);
    }

    return (
        <>
            {console.log(`controlblock render`)}
            <div className="control__block__title" onClick={(): void => {toggleActive(event);}}>
                <h1 className="control__block__title__text">ray fill/stroke</h1>
            </div>
            <div id={props.id} className="control__block__body" style={active === false ? {maxHeight: `0`} : {/*backgroundColor: `blue`, height: `${7.5*6}rem`*/maxHeight: `${(document.querySelector(`#${props.id}`) as Element).scrollHeight}px`}}> 

                <Control_ColorRanges
                    title="ray hsla limits A"
                    sliderFunction_H={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayFillHslaALimits/H`, value: [value[0],value[1]]});}}
                    sliderFunction_S={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayFillHslaALimits/S`, value: [value[0],value[1]]});}}
                    sliderFunction_L={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayFillHslaALimits/L`, value: [value[0],value[1]]});}}
                    sliderFunction_A={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayFillHslaALimits/A`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type: `randomizeHslaValues`, nameOfValueToRandomize: `rayFillHslaALimits`, value: drawingLimits.rayFillHslaALimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setRayFillHsla`, value:`A`});}}
                    mins={drawingLimits.rayFillHslaALimits_MINMAX}
                    maxs={drawingLimits.rayFillHslaALimits_MINMAX}
                    valueSources={drawingLimits.rayFillHslaALimits}
                ></Control_ColorRanges>

                <Control_ColorRanges
                    title="ray hsla limits B"
                    sliderFunction_H={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayFillHslaBLimits/H`, value: [value[0],value[1]]});}}
                    sliderFunction_S={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayFillHslaBLimits/S`, value: [value[0],value[1]]});}}
                    sliderFunction_L={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayFillHslaBLimits/L`, value: [value[0],value[1]]});}}
                    sliderFunction_A={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayFillHslaBLimits/A`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type: `randomizeHslaValues`, nameOfValueToRandomize: `rayFillHslaBLimits`, value: drawingLimits.rayFillHslaBLimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setRayFillHsla`, value:`B`});}}
                    mins={drawingLimits.rayFillHslaBLimits_MINMAX}
                    maxs={drawingLimits.rayFillHslaBLimits_MINMAX}
                    valueSources={drawingLimits.rayFillHslaBLimits}
                ></Control_ColorRanges>

                <ControlItemSlot title="ray gradient points"> 
                    <button className="control__item__button" onClick={(event): void => {
                        dispatchToDrawingSettings({type: `setRayGradientPoints`});
                    }}
                    >reset points</button>
                </ControlItemSlot>

                <Control_SingleRange
                    title="ray stroke width limits"
                    sliderFunction={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayStrokeWidthLimits`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type: `randomizePairedValues`, nameOfValueToRandomize: `rayStrokeWidthLimits`, value: drawingLimits.rayStrokeWidthLimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setRayStrokeWidth`});}}
                    min={drawingLimits.rayStrokeWidthLimits_MINMAX[0]}
                    max={drawingLimits.rayStrokeWidthLimits_MINMAX[1]}
                    valueSource={[drawingLimits.rayStrokeWidthLimits[0], drawingLimits.rayStrokeWidthLimits[1]]}
                ></Control_SingleRange>

                <Control_ColorRanges
                    title="ray stroke hsla limits"
                    sliderFunction_H={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayStrokeHslaLimits/H`, value: [value[0],value[1]]});}}
                    sliderFunction_S={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayStrokeHslaLimits/S`, value: [value[0],value[1]]});}}
                    sliderFunction_L={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayStrokeHslaLimits/L`, value: [value[0],value[1]]});}}
                    sliderFunction_A={(value: number[]): void => {dispatchToDrawingLimitsContext({type: `setPairedValues`, nameOfValueToSet: `rayStrokeHslaLimits/A`, value: [value[0],value[1]]});}}
                    buttonFunction_rndm={(): void => {dispatchToDrawingLimitsContext({type: `randomizeHslaValues`, nameOfValueToRandomize: `rayStrokeHslaLimits`, value: drawingLimits.rayStrokeHslaLimits_MINMAX});}}
                    buttonFunction_calc={(): void => {dispatchToDrawingSettings({type:`setRayStrokeHsla`});}}
                    mins={drawingLimits.rayStrokeHslaLimits_MINMAX}
                    maxs={drawingLimits.rayStrokeHslaLimits_MINMAX}
                    valueSources={drawingLimits.rayStrokeHslaLimits}
                ></Control_ColorRanges>


            </div>
        </>
    );
}