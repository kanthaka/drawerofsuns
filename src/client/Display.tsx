import React, { useContext, useMemo } from 'react';
import init, { SunSettingsInterface, RaySettingsInterface } from './init';
import { AppContext } from './App';
import { Random } from "random-js";
const random = new Random();

export const Display = React.memo(function Display(): JSX.Element {
    return (
        <div className="display__container">
            {console.log(`display render`)}
            <SunDrawing />
        </div>
    );
});

/**
 * 
 * https://github.com/facebook/react/issues/15156
 * 
 * components using useContents will re-render unless specific steps are taken to supress this behavior ... in the case below, i've implemented 'useMemo' on the component's return so that it will only re-render when [drawingSettings, dispatchToDrawingsSettings] is altered ... this will keep the drawing from flickering whenever a user moves a 'limits' slider (an action that changes the /limits/ state, not the /drawing/ state)
 * 
 */
/*const SunDrawing = React.memo(*/function SunDrawing(): JSX.Element {
    const appContext = useContext(AppContext);
    const drawingSettings = appContext.drawingSettings;
    const drawingBG = appContext.drawingBG;

    return useMemo((): JSX.Element => {
        return (
            <>
            {console.log(`sundrawing render`)}
            <svg className="sundrawing" xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 100">
                <defs>
                    <linearGradient
                        id={`gradient_drawingBG`}
                        gradientUnits="userSpaceOnUse"
                        x1={drawingBG.points.x1}
                        y1={drawingBG.points.y1}
                        x2={drawingBG.points.x2}
                        y2={drawingBG.points.y2}
                    >
                        <stop
                            id={`gradient_drawingBG_stopA`}
                            offset="0.0"
                            stopColor={drawingBG[`A`]/*init.getRandomColor(`hsla`)*/}
                        ></stop>
                        <stop
                            id={`gradient_drawingBG_stopB`}
                            offset="1.0"
                            stopColor={drawingBG[`B`]/*init.getRandomColor(`hsla`)*/}
                        ></stop>
                    </linearGradient>
                </defs>
                <rect
                    id="drawingBG"
                    x="-50"
                    y="-50"
                    width="200"
                    height="200"
                    fill={`url(#gradient_drawingBG)`}
                ></rect>
                {Array(drawingSettings.length).fill(1).map((item: number, index: number): number => {return index;}).map((item: number, index: number): JSX.Element => {
                    return <Sun key={`${String(item)}__`} sunNumber={index} sunSettings={drawingSettings[index]}/>;
                })}
            </svg>
            </>
        );
    }, [drawingSettings, drawingBG]);
}//);

function Sun(props: {key: string; sunNumber: number; sunSettings: SunSettingsInterface}): JSX.Element {
    return (
        <>
        {console.log(`sun render`)}
        {Array(props.sunSettings.rays.length).fill(1).map((item: number, index: number): number => {return index;}).map((item: number, index: number): JSX.Element => {
            return <RaySet key={`_${index}`} raySettings={props.sunSettings.rays[index]} centerPoint={props.sunSettings.centerPoint}/>;
        })}
        <Orb sunSettings={props.sunSettings}/>
        </>
    );
}

function computePointPair(a: number, b: number, modifier: number): string[] {
    // let modifier = 0.20;
    let pointPair: string[] = [(a+(b-a)*modifier).toFixed(2), (b-(b-a)*modifier).toFixed(2)];
    return pointPair;
}

/*const RaySet = React.memo(*/function RaySet(props: {key: string; raySettings: RaySettingsInterface; centerPoint: string}): JSX.Element {
    return useMemo((): JSX.Element => {
        return (
            <>
            <defs>
                <linearGradient
                    id={`gradient_${props.raySettings.raySetId}`}
                    gradientUnits="userSpaceOnUse"
                    x1={props.raySettings.rayGradientPoints.x1}
                    y1={props.raySettings.rayGradientPoints.x2}
                    x2={props.raySettings.rayGradientPoints.y1}
                    y2={props.raySettings.rayGradientPoints.y2}
                >
                    <stop
                        id={`gradient_${props.raySettings.raySetId}_stopA`}
                        offset="0.0"
                        stopColor={props.raySettings.rayFillColorA/*init.getRandomColor(`hsla`)*/}
                    ></stop>
                    <stop
                        id={`gradient_${props.raySettings.raySetId}_stopB`}
                        offset="1.0"
                        stopColor={props.raySettings.rayFillColorB/*init.getRandomColor(`hsla`)*/}
                    ></stop>
                </linearGradient>
            </defs>
            {console.log(`rayset render`)}
            {/* {console.log(props.raySettings)} */}
            {[`top`, `right`, `bottom`, `left`].map((sideItem, sideIndex): [] => {
                return props.raySettings.rayPoints[sideItem].map((item: number, index: number): JSX.Element => {
                    return index % 2 === 0
                    ?
                    <path
                        id="ray"
                        key={`ray_top${String(item)}_${random.string(6)}}`}
                        d = {sideItem ===  `top`
                            ? `M ${props.centerPoint} ${computePointPair(props.raySettings.rayPoints.top[index], props.raySettings.rayPoints.top[index+1], props.raySettings.rayPointOffsetModifier/100)[0]} -200, ${computePointPair(props.raySettings.rayPoints.top[index], props.raySettings.rayPoints.top[index+1], props.raySettings.rayPointOffsetModifier/100)[1]} -200 Z`
                                : sideItem === `right`
                                ? `M ${props.centerPoint}, 300 ${computePointPair(props.raySettings.rayPoints.right[index], props.raySettings.rayPoints.right[index+1], props.raySettings.rayPointOffsetModifier/100)[0]}, 300 ${computePointPair(props.raySettings.rayPoints.right[index], props.raySettings.rayPoints.right[index+1], props.raySettings.rayPointOffsetModifier/100)[1]} Z`
                                    : sideItem === `bottom`
                                    ? `M ${props.centerPoint}, ${computePointPair(props.raySettings.rayPoints.bottom[index], props.raySettings.rayPoints.bottom[index+1], props.raySettings.rayPointOffsetModifier/100)[0]} 300, ${computePointPair(props.raySettings.rayPoints.bottom[index], props.raySettings.rayPoints.bottom[index+1], props.raySettings.rayPointOffsetModifier/100)[1]} 300 Z`
                                        : sideItem === `left`
                                        ? `M ${props.centerPoint}, -200 ${computePointPair(props.raySettings.rayPoints.left[index], props.raySettings.rayPoints.left[index+1], props.raySettings.rayPointOffsetModifier/100)[0]}, -200 ${computePointPair(props.raySettings.rayPoints.left[index], props.raySettings.rayPoints.left[index+1], props.raySettings.rayPointOffsetModifier/100)[1]} Z`
                                            : null}
                        fill={`url(#gradient_${props.raySettings.raySetId})`}
                        stroke={props.raySettings.rayStrokeHsla}
                        strokeWidth={props.raySettings.rayStrokeWidth}
                        transform={`rotate(${props.raySettings.baseRotation}, ${props.centerPoint.split(` `)[0]}, ${props.centerPoint.split(` `)[1]})`}>
                        {props.raySettings.rayAnimate === true
                            ?
                            <animateTransform attributeName="transform"
                                attributeType="XML"
                                type="rotate"
                                from={`${props.raySettings.rayRotateDirection[0]} ${props.centerPoint}`}
                                to={`${props.raySettings.rayRotateDirection[1]} ${props.centerPoint}`}
                                dur={`${props.raySettings.raySetRotateDuration}s`}
                                repeatCount="indefinite" />
                            :
                            null}
                    </path>
                    :
                    null;
                });
            })}
            </>
        );
    }, [
        // props.key,
        props.centerPoint,
        // props.raySettings.rayUniformity,
        // props.raySettings.raysPerSide,
        // props.raySettings.rayPointGranularity,
        props.raySettings.raySetId,
        props.raySettings.rayPoints,
        props.raySettings.rayPointOffsetModifier,
        props.raySettings.rayFillColorA,
        props.raySettings.rayFillColorB,
        props.raySettings.rayGradientPoints,
        props.raySettings.rayStrokeHsla,
        props.raySettings.rayStrokeWidth,
        props.raySettings.baseRotation,
        props.raySettings.rayAnimate,
        props.raySettings.rayRotateDirection,
        props.raySettings.raySetRotateDuration
    ]);
}//);

/*const Orb = React.memo(*/function Orb(props: {sunSettings: SunSettingsInterface}): JSX.Element {
    return useMemo((): JSX.Element => {
        return (
            <>
            {console.log(`orb render`)}
            <circle
                id="orb"
                cx={props.sunSettings.centerPoint.split(` `)[0]} 
                cy={props.sunSettings.centerPoint.split(` `)[1]}
                r={props.sunSettings.orbSize}
                fill={props.sunSettings.orbColor}
                stroke={props.sunSettings.orbStrokeHsla/*init.getRandomColor(`rgb`)*/}
                strokeWidth={props.sunSettings.orbStrokeWidth} />
            </>
        );
    }, [props.sunSettings.centerPoint, props.sunSettings.orbSize, props.sunSettings.orbColor, props.sunSettings.orbStrokeHsla, props.sunSettings.orbStrokeWidth]);
}//);