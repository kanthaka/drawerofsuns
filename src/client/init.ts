import { Random } from "random-js";
const random = new Random();

interface RayPointsInterface {
    top: number[];
    right: number[];
    bottom: number[];
    left: number[];
}

export interface HslaLimitsInterface {
    H: number[];
    S: number[];
    L: number[];
    A: number[];
}

export interface DrawingLimitsInterface {
    sunCountLimits: number;
    sunCountLimits_MINMAX: number[];
    centerPointLimitsX: number[];
    centerPointLimitsX_MINMAX: number[];
    centerPointLimitsY: number[];
    centerPointLimitsY_MINMAX: number[];
    orbSizeLimits: number[];
    orbSizeLimits_MINMAX: number[];
    orbHslaLimits: {H: number[]; S: number[]; L: number[]; A: number[]};
    orbHslaLimits_MINMAX: {H: number[]; S: number[]; L: number[]; A: number[]};
    orbStrokeWidthLimits: number[];
    orbStrokeWidthLimits_MINMAX: number[];
    orbStrokeHslaLimits: {H: number[]; S: number[]; L: number[]; A: number[]};
    orbStrokeHslaLimits_MINMAX: {H: number[]; S: number[]; L: number[]; A: number[]};

    raysPerSideLimits: number[];
    raysPerSideLimits_MINMAX: number[];
    rayUniformityChanceLimits: number;
    rayUniformityChanceLimits_MINMAX: number[];
    rayPointOffsetModifierLimits: number[];
    rayPointOffsetModifierLimits_MINMAX: number[];

    rayFillHslaALimits: {H: number[]; S: number[]; L: number[]; A: number[]};
    rayFillHslaALimits_MINMAX: {H: number[]; S: number[]; L: number[]; A: number[]};
    rayFillHslaBLimits: {H: number[]; S: number[]; L: number[]; A: number[]};
    rayFillHslaBLimits_MINMAX: {H: number[]; S: number[]; L: number[]; A: number[]};
    rayGradientPointsLimits_MINMAX: number[];

    rayStrokeWidthLimits: number[];
    rayStrokeWidthLimits_MINMAX: number [];

    rayStrokeHslaLimits: {H: number[]; S: number[]; L: number[]; A: number[]};
    rayStrokeHslaLimits_MINMAX: {H: number[]; S: number[]; L: number[]; A: number[]};

    raySetCountLimits: number[];
    raySetCountLimits_MINMAX: number[];
    rayAnimateChanceLimits: number;
    rayAnimateChanceLimits_MINMAX: number[];
}

export interface RaySettingsInterface {
    raySetId: string;
    raysPerSide: number;
    rayUniformity: boolean;
    rayPointGranularity: number;
    rayPoints: RayPointsInterface;
    rayPointOffsetModifier: number;

    rayFillColorA: string;
    rayFillColorB: string;
    rayGradientPoints: {
        x1: number;
        y1: number;
        x2: number;
        y2: number;
    };

    rayStrokeHsla: string;
    rayStrokeWidth: number;
    baseRotation: number;
    rayAnimate: boolean;
    rayRotateDirection: number[];
    raySetRotateDuration: number;
}

export interface SunSettingsInterface {
    id: string;
    centerPoint: string;
    orbSize: number;
    orbColor: string;
    orbStrokeWidth: number;
    orbStrokeHsla: string;
    rays: RaySettingsInterface[];
}

export const raysPerSideList = [1,2,3,4,5,6,8,10,15,20,25,30,40,50,75,100];
export const strokeWidthList = [0, 6, 8, 12, 16, 20, 24, 32, 36, 40, 48, 64, 128, 256, 512];


export default {
    sortIntegerArray(array: number[]): number[] {
        return array.sort((x: number, y: number): number => {return x -y;});
    },
    getRandomPoint(xmin: number, xmax: number, ymin: number, ymax: number): string {
        return `${random.integer(xmin, xmax)} ${random.integer(ymin, ymax)}`;
    },
    getRandomHslaWithLimits(limitSource: HslaLimitsInterface): string {
        return `hsla(${
            random.integer(limitSource[`H`][0], limitSource[`H`][1])
        }, ${
            random.integer(limitSource[`S`][0], limitSource[`S`][1])
        }%, ${
            random.integer(limitSource[`L`][0], limitSource[`L`][1])
        }%, ${
            random.integer(limitSource[`A`][0], limitSource[`A`][1])/100
        })`;
    },
    getRandomColor(type: string): string {
        let tempValue = ``;
        switch (type) {
            case `hsla`:
                tempValue = `hsla(${
                    random.integer(0, 359)
                }, ${
                    random.integer(0, 100)
                }%, ${
                    random.integer(0, 100)
                }%, ${
                    random.integer(0, 100)/100
                })`;
                break;
            case `rgba_alpha-20-80`:
                tempValue = `rgba(${random.integer(0,255)}, ${random.integer(0,255)}, ${random.integer(0,255)}, ${random.integer(20,80)/100})`;
                break;
            case `rgba`:
                tempValue = `rgba(${random.integer(0,255)}, ${random.integer(0,255)}, ${random.integer(0,255)}, ${random.integer(0,100)/100})`;
                break;
            case `rgb`:
                tempValue =  `rgb(${random.integer(0,255)}, ${random.integer(0,255)}, ${random.integer(0,255)})`;
                break;
        }
        return tempValue;
    },
    createArrayOfRayPoints(raysPerSide: number, rayUnifomity: boolean, rayPointGranularity: number): number[] {
        let tempArray = Array(raysPerSide*2).fill(0);
        if (rayUnifomity === false) {
            for (let i=0; i<tempArray.length; i++) {
                let tempNumber: number | undefined;
                if (500/rayPointGranularity >= raysPerSide) {
                    while (tempNumber === undefined || tempArray.includes(tempNumber)) {
                        tempNumber = (random.integer(0,500/rayPointGranularity)*rayPointGranularity)-200;
                    }
                    tempArray[i] = tempNumber;
                } else {
                    console.log(`!!! - rayPointGranularity is not fine enough - !!!`);
                }
            }
        } else {
            let spacing = 500/(raysPerSide);
            for (let i=0; i<tempArray.length/2; i++) {
                tempArray[i*2] = (i*spacing-200);
                tempArray[i*2+1] = (((i*spacing)-200)+spacing);
            }
        }
        tempArray = this.sortIntegerArray(tempArray);
        // console.log(tempArray);
        return tempArray;
    },
    buildRayPoints(rayPoints: RayPointsInterface, rayUniformity: boolean, rayPointGranularity: number, raysPerSide: number, previousRayUniformity: boolean | null = null): RayPointsInterface {
        let tempRayPoints: RayPointsInterface;

        if (rayUniformity === true) {
            rayPointGranularity = 500/raysPerSide;
        } else {
            if (raysPerSide === 1 && rayPointGranularity === 500) {
                rayPointGranularity = rayPointGranularity/2; // <-- UNREACHABLE CODE?
            } else {
                rayPointGranularity = 50/raysPerSide;//1;
            }
        }

        if (previousRayUniformity === null || previousRayUniformity === false || rayUniformity === false) {

            tempRayPoints = {
                top: this.createArrayOfRayPoints(raysPerSide, rayUniformity, rayPointGranularity),
                right: this.createArrayOfRayPoints(raysPerSide, rayUniformity, rayPointGranularity),
                bottom: this.createArrayOfRayPoints(raysPerSide, rayUniformity, rayPointGranularity),
                left: this.createArrayOfRayPoints(raysPerSide, rayUniformity, rayPointGranularity),
            };
        } else {
            tempRayPoints = rayPoints;
        }
        return tempRayPoints;
    },
    buildRaySettings(drawingLimits: DrawingLimitsInterface): RaySettingsInterface {

        let raySettings: RaySettingsInterface = {
            raySetId: random.uuid4(),
            raysPerSide: 0,
            rayUniformity: true,
            rayPointGranularity: 0,
            rayPoints: {
                top: [],
                right: [],
                bottom: [],
                left: []
            },
            rayPointOffsetModifier: 0,
            rayFillColorA: ``,
            rayFillColorB: ``,
            rayGradientPoints: {
                x1: random.integer(drawingLimits.rayGradientPointsLimits_MINMAX[0], drawingLimits.rayGradientPointsLimits_MINMAX[1]),
                y1: random.integer(drawingLimits.rayGradientPointsLimits_MINMAX[0], drawingLimits.rayGradientPointsLimits_MINMAX[1]),
                x2: random.integer(drawingLimits.rayGradientPointsLimits_MINMAX[0], drawingLimits.rayGradientPointsLimits_MINMAX[1]),
                y2: random.integer(drawingLimits.rayGradientPointsLimits_MINMAX[0], drawingLimits.rayGradientPointsLimits_MINMAX[1]),
            },
            rayStrokeHsla: this.getRandomHslaWithLimits(drawingLimits.rayStrokeHslaLimits),
            rayStrokeWidth: 0,
            baseRotation: 0,
            rayAnimate: false,
            rayRotateDirection: [0 ,0],
            raySetRotateDuration: 0
        };

        raySettings.raysPerSide = raysPerSideList[random.integer(drawingLimits.raysPerSideLimits[0], drawingLimits.raysPerSideLimits[1])];

        raySettings.rayUniformity = random.bool(drawingLimits.rayUniformityChanceLimits/100);

        raySettings.raysPerSide === 1 ? raySettings.rayPointGranularity = 50 : raySettings.rayPointGranularity = 500/raySettings.raysPerSide;

        raySettings.rayPoints = this.buildRayPoints(raySettings.rayPoints, raySettings.rayUniformity, raySettings.rayPointGranularity, raySettings.raysPerSide, this.initialized);

        raySettings.rayPointOffsetModifier = random.integer(drawingLimits.rayPointOffsetModifierLimits[0], drawingLimits.rayPointOffsetModifierLimits[1]);

        raySettings.rayFillColorA = this.getRandomHslaWithLimits(drawingLimits.rayFillHslaALimits),

        raySettings.rayFillColorB = this.getRandomHslaWithLimits(drawingLimits.rayFillHslaBLimits),

        raySettings.rayStrokeHsla = this.getRandomColor(`rgb`);

        raySettings.rayStrokeWidth = strokeWidthList[random.integer(drawingLimits.rayStrokeWidthLimits[0], drawingLimits.rayStrokeWidthLimits[1])]/100,

        raySettings.baseRotation = random.integer(0 , 360);

        raySettings.rayAnimate = random.bool(drawingLimits.rayAnimateChanceLimits/100);//false;//

        raySettings.rayRotateDirection = random.pick([
            [raySettings.baseRotation, raySettings.baseRotation+360],
            [raySettings.baseRotation, raySettings.baseRotation-360],
        ]);
        raySettings.raySetRotateDuration = random.integer(60, 180);

        return raySettings;
    },  
    buildSunSettings(drawingLimits: DrawingLimitsInterface): SunSettingsInterface {

        let sunSettings: SunSettingsInterface = {
            id: random.uuid4(),
            centerPoint: `0 0`,
            orbSize: 0,
            orbColor: this.getRandomHslaWithLimits(drawingLimits.orbHslaLimits),

            orbStrokeWidth: 0,
            orbStrokeHsla: this.getRandomHslaWithLimits(drawingLimits.orbStrokeHslaLimits),

            rays: Array(random.integer(drawingLimits.raySetCountLimits[0],drawingLimits.raySetCountLimits[1])).fill(0).map((item, index): RaySettingsInterface => {
                return this.buildRaySettings(drawingLimits);
            })
        };

        sunSettings.orbStrokeWidth = strokeWidthList[random.integer(drawingLimits.orbStrokeWidthLimits[0], drawingLimits.orbStrokeWidthLimits[1])]/100,

        sunSettings.centerPoint = this.getRandomPoint(drawingLimits.centerPointLimitsX[0], drawingLimits.centerPointLimitsX[1], drawingLimits.centerPointLimitsY[0], drawingLimits.centerPointLimitsY[1]);
        sunSettings.orbSize = random.integer(drawingLimits.orbSizeLimits[0], drawingLimits.orbSizeLimits[1]);

        return sunSettings;
    },
    buildDrawingSettings(drawingLimits: DrawingLimitsInterface): SunSettingsInterface[] {

        return Array(drawingLimits.sunCountLimits).fill(0).map((item, index): SunSettingsInterface => {
            return this.buildSunSettings(drawingLimits);
        });
    },
    buildDrawingBG(drawingBGLimits): {A: string; B: string; points: {x1: number; y1: number; x2: number; y2: number}} {
        return {
            A: this.getRandomHslaWithLimits(drawingBGLimits[`A`]),

            B: this.getRandomHslaWithLimits(drawingBGLimits[`B`]),

            points: {
                x1: random.integer(-50, 150),
                y1: random.integer(-50, 150),
                x2: random.integer(-50, 150),
                y2: random.integer(-50, 150),
            }
        };
    }
};