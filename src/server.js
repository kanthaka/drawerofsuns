// const admin = require(`firebase-admin`);
const express = require(`express`);
const path = require(`path`);
const cors = require(`cors`);

const app = express();

const port = process.env.PORT || 3001;

// const srvAcnt = require(`./cert.json`);

// console.log(srvAcnt);
// console.log(JSON.parse(process.env.CERT));

// http://blog.dylanjpierce.com/tutorial/google/google-api/environment-variables/2018/10/03/authenticate-google-service-account-with-an-environment-variable.html

// CERT="$(< ./cert.json)" nodemon server.js

// console.log(process.env.CERT);

// admin.initializeApp({
//     credential: admin.credential.cert(JSON.parse(process.env.CERT))
// });

// const db = admin.firestore();

// admin.firestore().getCollections().then((asdf) => {console.log(asdf);});

// db.collection("simpleFullStack")
//     .get()
//     .then(snapshot => {
//         // console.log(typeof snapshot);
//         // console.log(Object.keys(snapshot));
//         // console.log(snapshot._materializedDocs);
//         snapshot.forEach(doc => {CCERT
//             console.log(doc);
//             console.log(doc.id);
//             console.log(doc.id, "=>", doc.data());
//         });
//     })
//     .catch(err => {
//         console.log("Error getting documents", err);
//     });

// db.collection(`simpleFullStack`).doc(`pbAl8UOw7n3iosqbZ3zA`).get().then((doc) => {console.log(doc.data())});

console.log(process.env.PORT);

const corsOptions = {
    origin: (process.env.PORT ? false : `http://localhost:3000`),//`http://localhost:3000`,//
    optionsSuccessStatus: 200
};

app.use(cors(corsOptions));

app.use(express.static(path.join(__dirname, `public`)));

app.get(`/msg`, (request, response) => {
    const message = {
        message: `!!!hello world!!!`
    };
    response.json(message);
    // db.collection(`simpleFullStack`).doc(`pbAl8UOw7n3iosqbZ3zA`)
    //     .get()
    //     .then((doc) => {
    //         response.json(doc.data());
    //     })
    //     .catch((err) => {
    //         console.log(`Error getting documents`, err);
    //     });
});

app.listen(port);